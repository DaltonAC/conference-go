from .keys import OPEN_WEATHER_API_KEY, PEXELS_API_KEY
import json
import requests


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"per_page": 1, "query": city + " " + state}
    response = requests.get(url, headers=headers, params=params)
    content = json.loads(response.content)

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    key = OPEN_WEATHER_API_KEY
    latlon_response = requests.get(
        f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state}&appid={key}",
        timeout=5,
    )
    geo_data = json.loads(latlon_response.content)
    latitude = geo_data[0].get("lat")
    longitude = geo_data[0].get("lon")

    response = requests.get(
        f"https://api.openweathermap.org/data/2.5/weather?lat={latitude}&lon={longitude}&appid={key}&units=imperial",
        timeout=5,
    )
    content = response.content
    weather_data = json.loads(content)
    temperature = weather_data.get("main").get("temp")
    description = weather_data.get("weather")[0].get("description")
    weather = {
        "temp": temperature,
        "description": description,
    }

    return weather if bool(content) else None
