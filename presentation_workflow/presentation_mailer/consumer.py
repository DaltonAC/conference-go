import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    body_data = json.loads(body)
    name = body_data.get("presenter_name")
    recipients = [body_data.get("presenter_email")]
    title = body_data.get("title")
    subject = "Your presentation has been accepted"
    message_body = f"{name}, we're happy to tell you that your presentation {title} has been accepted"
    send_mail(
        subject=subject,
        message=message_body,
        from_email="admin@conference.go",
        recipient_list=recipients,
        fail_silently=False,
    )


def process_rejection(ch, method, properties, body):
    body_data = json.loads(body)
    name = body_data.get("presenter_name")
    recipients = [body_data.get("presenter_email")]
    title = body_data.get("title")
    subject = "Your presentation has been rejected"
    message_body = f"{name}, we're saddened to tell you that your presentation {title} has been rejected"
    send_mail(
        subject=subject,
        message=message_body,
        from_email="admin@conference.go",
        recipient_list=recipients,
        fail_silently=False,
    )


parameters = pika.ConnectionParameters(host="rabbitmq")
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue="presentation_approvals")
channel.basic_consume(
    queue="presentation_approvals",
    on_message_callback=process_approval,
    auto_ack=True,
)
channel.start_consuming()

parameters = pika.ConnectionParameters(host="rabbitmq")
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue="presentation_rejections")
channel.basic_consume(
    queue="presentation_rejections",
    on_message_callback=process_rejection,
    auto_ack=True,
)
channel.start_consuming()
